package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Sala1  {


    @FXML
    private Button cancelar;
    @FXML
    private Button pagar;
    private float preciodia;
    private List<Asientos> todoslosAsientos=new ArrayList<>();
    private  List<Asientos> asientosSeleccionados =new ArrayList<>();
    @FXML
    private Label nombrePelicula;
    @FXML
    private Label tiempoDuracion;
    @FXML
    private Label diaPelicula;
    private  String usuario="";




    @FXML
    void cancelar(ActionEvent event) {
        if (!cancelar.isPressed()){
            Node source=(Node) event.getSource();
            Stage stage=(Stage) source.getScene().getWindow();
            stage.close();
        }
    }

    @FXML
    public void obtenerPrecioDia(String precio){

            switch (precio) {
                case "Sa  25/12", "Vie 31 /12" -> preciodia = 8;
                case "Do 26/12", "Lun 27/12", "Jue 30/12", "Mar 28/12" -> preciodia = 6;
                case "Mier 29 /12" -> preciodia = 5;

        }
    }


    @FXML
    void pagar(ActionEvent event) {
        if (!asientosSeleccionados.isEmpty()) {
            try {
                Stage stage = new Stage(StageStyle.DECORATED);
                stage.initModality(Modality.APPLICATION_MODAL);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("Recibo.fxml"));
                Parent root = loader.load();
                Recibo controlador = loader.getController();
                controlador.introducirNumeroButacas(asientosSeleccionados.size(), preciodia, asientosSeleccionados,usuario);


                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("");

                stage.showAndWait();
                if (controlador.getPagado()) {

                    for (Asientos asientosSeleccionado : asientosSeleccionados) {
                        if ((asientosSeleccionado.getAsientos().equals(Asientos.estadoAsiento.ASIENTOSPARACOMPRAR))) {
                            asientosSeleccionado.asientoReservado();
                        }else {
                            asientosSeleccionados.remove(asientosSeleccionado);
                        }

                    }
                    asientosSeleccionados.clear();

                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void initialize(){
        System.out.println("Ha entrado al initialize");
        anyadirAsientos();
        System.out.println("Ha salido al initialize");

    }

    @FXML
    void cambiarEstadosBotones(ActionEvent event) {
        Button botones=(Button) event.getSource();
        System.out.println(botones.getId());
        for (Asientos todoslosAsiento : todoslosAsientos) {
            if (todoslosAsiento.getId().equals(botones.getId())) {
                todoslosAsiento.setAsignacionBoton(botones);
                todoslosAsiento.cambiarEstadoAsiento();

                if (todoslosAsiento.getAsientos().equals(Asientos.estadoAsiento.ASIENTOSPARACOMPRAR)){
                    asientosSeleccionados.add(todoslosAsiento);
                }else {
                    asientosSeleccionados.remove(todoslosAsiento);
                }


            }

        }


    }

    public void anyadirAsientos(){

        for (int i = 1; i <=12 ; i++) {
            for (int j = 1; j <= 18; j++) {
                todoslosAsientos.add(new Asientos(i,j,"b"+i+"f"+j));

            }
        }
        System.out.println("fin anyadir botones");
    }


    public void setPelicula(Pelicula pelicula,String nombreUsuario) {
        nombrePelicula.setText(pelicula.getTitulo());
        tiempoDuracion.setText(pelicula.getDuracion()+"mins.");
        usuario=nombreUsuario;

    }

    @FXML
    public void asignarDia(String dia,String hora) {
        switch (dia) {
            case "Sa  25/12" -> diaPelicula.setText("Sabado  25/12/2021 a las "+hora);
            case "Vie 31 /12" -> diaPelicula.setText("Viernes  31/12/2021 a las "+hora);
            case "Do 26/12" -> diaPelicula.setText("Domingo  26/12/2021 a las "+hora);
            case "Lun 27/12" -> diaPelicula.setText("Lunes  27/12/2021 a las "+hora);
            case "Jue 30/12" -> diaPelicula.setText("Jueves  30/12/2021 a las "+hora);
            case "Mar 28/12" -> diaPelicula.setText("Martes  28/12/2021 a las "+hora);
            case "Mier 29 /12" -> diaPelicula.setText("Miercoles  29/12/2021 a las "+hora);
        }


    }
}

