package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class LoginController {
    @FXML
    private Label Registrarse;

    @FXML
    private Label Username;

    @FXML
    private Button botonLogin;

    @FXML
    private Button botonRegistrarseLogin;

    @FXML
    private Label contrasenya;

    @FXML
    private ImageView imagenLogo;

    @FXML
    private TextField textareaContraseña;

    @FXML
    private TextField  textareaUsername;

    @FXML
    public void initialize() {
        this.imagenLogo.setImage(new Image("https://www.itemformacion.com/img/netlog3.png"));

    }

    @FXML
    void botonLogearse(ActionEvent event) {
        if (!(textareaUsername.getText().equals("")) && !(textareaContraseña.getText().equals("")) && !(textareaUsername.getText().contains(" ")&& !(textareaContraseña.getText().contains(" ")))){

            try {


                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);

                FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));
                Parent root = loader.load();
                HelloController controlador = loader.getController();
                controlador.anyadirUsuario(textareaUsername.getText());



                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("");
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Campo Vacio");
            alert.setContentText("Error en la aplicacion hay algun campo vacio");
            alert.showAndWait();
        }

    }

    @FXML
    void botonRegistrarse(ActionEvent event) {
        try {
            Stage stage=new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            Parent root=FXMLLoader.load(Objects.requireNonNull(getClass().getResource("Registrarse.fxml")));


            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
