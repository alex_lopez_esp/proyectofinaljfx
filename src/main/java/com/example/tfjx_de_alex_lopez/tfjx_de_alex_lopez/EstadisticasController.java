package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.*;

public class EstadisticasController {
    @FXML
    private BarChart<String, Number> EstadisticasBarras;

    @FXML
    private PieChart estadisticaRedondas;


    @FXML
    void initialize(){
        PieChartInit();
        BarChartInit();



    }

    void PieChartInit(){
        ObservableList<PieChart.Data> pieChartData= FXCollections.observableArrayList(
                new PieChart.Data("La pantera Rosa 2",(Math.random()*100)),
                new PieChart.Data("Avatar",(Math.random()*100)),
                new PieChart.Data("Acero Puro",(Math.random()*100)),
                new PieChart.Data("SCRE4M",(Math.random()*100)),
                new PieChart.Data("CazaFantasmas",(Math.random()*100)),
                new PieChart.Data("Harry potter y las reliquias",(Math.random()*100)));
        estadisticaRedondas.setData(pieChartData);



        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        BarChart<String,Number> bc =
                new BarChart<>(xAxis,yAxis);
        bc.setTitle("Informacion Peliculas");
        xAxis.setLabel("Peliculas");
        yAxis.setLabel("Compradas");

        XYChart.Series pelicula = new XYChart.Series();
        pelicula.setName("La pantera Rosa 2");
        pelicula.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));

        XYChart.Series pelicula2 = new XYChart.Series();
        pelicula2.setName("Avatar");
        pelicula2.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));

        XYChart.Series pelicula3 = new XYChart.Series();
        pelicula3.setName("Acero Puro");
        pelicula3.getData().add(new XYChart.Data("Acero Puro", (Math.random()*100)));

        XYChart.Series pelicula4 = new XYChart.Series();
        pelicula4.setName("SCRE4M");
        pelicula4.getData().add(new XYChart.Data("SCRE4M", (Math.random()*100)));

        XYChart.Series pelicula5 = new XYChart.Series();
        pelicula5.setName("CazaFantasmas");
        pelicula5.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));

        XYChart.Series pelicula6 = new XYChart.Series();
        pelicula6.setName("Harry potter y las reliquias");
        pelicula6.getData().add(new XYChart.Data("Harry potter y las reliquias", (Math.random()*100)));



        bc.getData().addAll(pelicula, pelicula2,pelicula3,pelicula4,pelicula5,pelicula6);

    }
    void  BarChartInit(){
        XYChart.Series peliculas = new XYChart.Series();
        peliculas.setName("Sabado");
        peliculas.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        peliculas.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        peliculas.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100)));
        peliculas.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        peliculas.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        peliculas.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula2 = new XYChart.Series();
        pelicula2.setName("Domingo");
        pelicula2.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula2.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula2.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula2.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula2.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula2.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula3 = new XYChart.Series();

        pelicula3.setName("Lunes");
        pelicula3.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula3.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula3.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula3.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula3.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula3.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula4 = new XYChart.Series();
        pelicula4.setName("Martes");
        pelicula4.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula4.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula4.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula4.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula4.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula4.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula5 = new XYChart.Series();
        pelicula5.setName("Miercoles");
        pelicula5.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula5.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula5.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula5.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula5.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula5.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula6 = new XYChart.Series();
        pelicula6.setName("Jueves");
        pelicula6.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula6.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula6.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula6.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula6.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula6.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        XYChart.Series pelicula7 = new XYChart.Series();
        pelicula7.setName("Viernes");
        pelicula7.getData().add(new XYChart.Data("La pantera Rosa 2", (Math.random()*100)));
        pelicula7.getData().add(new XYChart.Data("Avatar", (Math.random()*100)));
        pelicula7.getData().add(new XYChart.Data("Acero Puro",(Math.random()*100) ));
        pelicula7.getData().add(new XYChart.Data("SCRE4M",(Math.random()*100) ));
        pelicula7.getData().add(new XYChart.Data("CazaFantasmas", (Math.random()*100)));
        pelicula7.getData().add(new XYChart.Data(  "Harry potter y las reliquias", (Math.random()*100)));

        EstadisticasBarras.getData().addAll(peliculas, pelicula2, pelicula3, pelicula4, pelicula5, pelicula6, pelicula7);

    }


}
