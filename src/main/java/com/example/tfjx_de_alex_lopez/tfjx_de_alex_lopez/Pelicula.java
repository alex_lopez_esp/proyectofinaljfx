package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

public class Pelicula {
    private String titulo;
    private String pathImage;
    private String paisyAnyo;
    private String director;
    private String reparto;
    private int duracion;
    private Sala1 sala;

    public Pelicula(String titulo, String pathImage, String paisyAnyo, String director, String reparto, int duracion,Sala1 sala) {
        this.titulo = titulo;
        this.pathImage = pathImage;
        this.paisyAnyo = paisyAnyo;
        this.director = director;
        this.reparto = reparto;
        this.duracion = duracion;
        this.sala=sala;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getPaisyAnyo() {
        return paisyAnyo;
    }

    public void setPaisyAnyo(String paisyAnyo) {
        this.paisyAnyo = paisyAnyo;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
}
