package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Recibo {

    @FXML
    private Text butacasCompradas;

    @FXML
    private ImageView imagen1;

    @FXML
    private ImageView imagen2;

    @FXML
    private Text numeroEntradas;

    @FXML
    private TextArea butacasSeleccionadas;


    @FXML
    private Label precioTotal;

    @FXML
    private Label €;
    @FXML
    private Label nombreUsuario;
    @FXML
    private Button botonCancelar;

    @FXML
    private Button botonPagar;

    boolean estaPagado;


    int contador;



    @FXML
    public  void introducirNumeroButacas(int i, float precio, List<Asientos> asientosSeleccionados,String nombreUsuario){
        estaPagado=false;
        imagen1.setImage(new Image("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/072fa096-b852-4161-ae5d-8f42f037c051/d4ynhs2-8410ceef-e217-40a9-9e81-ed35a98932af.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzA3MmZhMDk2LWI4NTItNDE2MS1hZTVkLThmNDJmMDM3YzA1MVwvZDR5bmhzMi04NDEwY2VlZi1lMjE3LTQwYTktOWU4MS1lZDM1YTk4OTMyYWYuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.OkHlP0qiYFekq8jd3sbCz2KjWea8J0FiXW2lNSPVz5Y"));
        imagen2.setImage(new Image("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/072fa096-b852-4161-ae5d-8f42f037c051/d4ynhs2-8410ceef-e217-40a9-9e81-ed35a98932af.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzA3MmZhMDk2LWI4NTItNDE2MS1hZTVkLThmNDJmMDM3YzA1MVwvZDR5bmhzMi04NDEwY2VlZi1lMjE3LTQwYTktOWU4MS1lZDM1YTk4OTMyYWYuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.OkHlP0qiYFekq8jd3sbCz2KjWea8J0FiXW2lNSPVz5Y"));
        System.out.println("DESPUES DE ENTRAR \n"+contador);
        this.contador=i;
        numeroEntradas.setText(String.valueOf(i));
        butacasCompradas.setText(String.valueOf(precio));
        precioTotal.setText(String.valueOf(i*precio));
        butacasSeleccionadas.setEditable(false);
        butacasSeleccionadas.setStyle("-fx-background-color:  #4EB2C7");
        for (Asientos asientosSeleccionado : asientosSeleccionados) {
            butacasSeleccionadas.setText(butacasSeleccionadas.getText() + "\nNumero de butaca: " + asientosSeleccionado.getAsignacionBoton().getId());

        }
        this.nombreUsuario.setText(nombreUsuario);

    }
    @FXML
    void presionarCancelar(ActionEvent event) {
        Node source=(Node) event.getSource();
        Stage stage=(Stage) source.getScene().getWindow();
        stage.close();


    }

    @FXML
    void presionarPagar(ActionEvent event) {
        Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setTitle("Confirmacion");
        alert.setContentText("¿Deseas realizar el pago?");
       Optional<ButtonType>action= alert.showAndWait();

       if (action.get()==ButtonType.OK){
           System.out.println("adios");
           estaPagado=true;


           Node source=(Node) event.getSource();
           Stage stage=(Stage) source.getScene().getWindow();
           stage.close();

       }


    }

    public boolean getPagado() {

        return estaPagado;
    }
}
