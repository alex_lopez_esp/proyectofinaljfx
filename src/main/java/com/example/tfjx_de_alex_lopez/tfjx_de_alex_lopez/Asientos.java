package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.scene.control.Button;

public class Asientos {
    private final String CAMBIARCOLORRESERVADO="-fx-background-color:  red";
    private final String CAMBIARCOLORCOMPRAR="-fx-background-color:  yellow";
    private final String CAMBIARCOLORLIBRE="-fx-background-color:   #00C117";




    enum estadoAsiento {
        ASIENTOSLIBRE,ASIENTOSPARACOMPRAR,ASIENTORESERVADO
    }

    private int fila;

    private int columna;
    private String id;
    private estadoAsiento asientos;
    private String estilo;
    private Button asignacionBoton;

    public Asientos(int fila, int columna, String id) {
        this.fila = fila;
        this.columna = columna;
        this.id = id;
        this.asientos= estadoAsiento.ASIENTOSLIBRE;
        this.estilo=CAMBIARCOLORLIBRE;
//      this.asignacionBoton=button;



    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public estadoAsiento getAsientos() {
        return asientos;
    }

    public void setAsientos(estadoAsiento asientos) {
        this.asientos = asientos;
    }


    public void asignarBoton(Button boton){
        this.asignacionBoton=boton;

    }

    public Button getAsignacionBoton() {
        return asignacionBoton;
    }

    public void setAsignacionBoton(Button asignacionBoton) {
        this.asignacionBoton = asignacionBoton;
    }


    public void asientoParacomprar(){


        System.out.println("ha entrado en el amarillo");
        this.asientos=estadoAsiento.ASIENTOSPARACOMPRAR;
        this.estilo=CAMBIARCOLORCOMPRAR;
        this.asignacionBoton.setStyle(estilo);

        System.out.println(asignacionBoton.getStyle());

    }

    public void asientoReservado(){
        System.out.println("ha entrado en el rojo");
        this.asientos=estadoAsiento.ASIENTORESERVADO;
        this.estilo=CAMBIARCOLORRESERVADO;
        this.asignacionBoton.setStyle(estilo);

    }

    public void asientoLibre() {
        System.out.println("ha entrado en el verde");
        this.asientos=estadoAsiento.ASIENTOSLIBRE;
        this.estilo=CAMBIARCOLORLIBRE;
        this.asignacionBoton.setStyle(estilo);
    }


    public void cambiarEstadoAsiento(){

        if (this.getAsientos().equals(estadoAsiento.ASIENTOSLIBRE)){
            this.asientoParacomprar();

        }else if (this.getAsientos().equals(estadoAsiento.ASIENTORESERVADO)){
            this.asientoReservado();

        }else {
            this.asientoLibre();

        }


    }





}
