package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;


public class MostrarPeliculas {

    @FXML
    private ImageView imagenPelicula;

    @FXML
    private Label nombrePelicula;
    @FXML
    private Label minutos;
    @FXML
    private TextArea descripcion;
    @FXML
    private ImageView imagenReloj;

    @FXML
    private ImageView imagenEstadisticas;

    Pelicula pelicula;
    @FXML
    private Button dia1;

    @FXML
    private Button dia2;

    @FXML
    private Button dia3;

    @FXML
    private Button dia4;

    @FXML
    private Button dia5;

    @FXML
    private Button dia6;

    @FXML
    private Button dia7;
    private String dia="";
    private  String usuario="";







    public void setPelicula(Pelicula pelicula) {
        this.pelicula=pelicula;
        imagenPelicula.setImage(new Image(pelicula.getPathImage()));
        nombrePelicula.setText(pelicula.getTitulo());
        minutos.setText(pelicula.getDuracion()+"min.");
        descripcion.setText(pelicula.getReparto());
        descripcion.setDisable(false);
       descripcion.setEditable(false);
       imagenReloj.setImage(new Image("https://docplayer.es/docs-images/101/148839355/images/1-3.jpg"));
       imagenEstadisticas.setImage(new Image("https://reygif.com/media/5/barras-estadisticas-62291.gif"));
    }


    public void elegirhora(ActionEvent event) {
        Button boton=(Button) event.getSource();

        try {


            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("sala1.fxml"));
            Parent root = loader.load();
            Sala1 controlador = loader.getController();

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.setTitle("");
            if (dia.equals("")){
              dia="Sa  25/12";
            }
            controlador.obtenerPrecioDia(dia);
            controlador.asignarDia(dia,boton.getText());
            controlador.setPelicula(pelicula,usuario);
            stage.show();





        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void elegirDia1(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia1.getText();
        System.out.println(dia);

        dia1.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia2(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia2.getText();
        System.out.println(dia);
        dia2.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia3(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia3.getText();
        System.out.println(dia);
        dia3.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia4(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia4.getText();
        System.out.println(dia);
        dia4.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia5(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia5.getText();
        System.out.println(dia);
        dia5.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia6(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia6.getText();
        System.out.println(dia);
        dia6.setStyle("-fx-background-color: #4EB2C7");
    }

    public void elegirDia7(ActionEvent event) {
        ponerBotonesColorDefault();
        dia=dia7.getText();
        System.out.println(dia);
        dia7.setStyle("-fx-background-color: #4EB2C7");
    }

    public void ponerBotonesColorDefault(){
        dia1.setStyle("");
        dia2.setStyle("");
        dia3.setStyle("");
        dia4.setStyle("");
        dia5.setStyle("");
        dia6.setStyle("");
        dia7.setStyle("");


    }
    @FXML
    void anyadirUsuario(String nombreUsuario){
        this.usuario=nombreUsuario;
    }

    @FXML
    void abrirEstadisticas(MouseEvent event) {
        try {

        System.out.println("Se ha presionado");
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Estadisticas.fxml"));
        Parent root = loader.load();
        EstadisticasController controlador = loader.getController();

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("");

        stage.show();
    } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
