package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class RegistrarseController {

    @FXML
    private Label Registrarse;

    @FXML
    private Label Username;

    @FXML
    private Button bottonRegistrarse;

    @FXML
    private Label contrasenya;

    @FXML
    private Label contrasenya1;

    @FXML
    private Label contrasenya11;

    @FXML
    private Label contrasenya111;

    @FXML
    private TextField textareaContrasenya;

    @FXML
    private TextField textareaCorreoElectronico;

    @FXML
    private TextField textareaNombreYApellidos;

    @FXML
    private TextField textareaRepetirContrasenya;


    @FXML
    private TextField textareaUsername;



    @FXML
    void registrarUsuario(ActionEvent event) {
        if (!(textareaUsername.getText().equals("")) && !(textareaContrasenya.getText().equals("")) && !(textareaCorreoElectronico.getText().equals("") && !(textareaNombreYApellidos.getText().equals("") && !(textareaRepetirContrasenya.getText().equals(""))))){
            if (!(textareaContrasenya.getText().equals(textareaRepetirContrasenya.getText()))){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Contraseña Erronea");
                alert.setContentText("Error las contraseñas introducidas no conciden");
                alert.showAndWait();
            }else {

                try {


                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));
                    Parent root = loader.load();
                    HelloController controlador = loader.getController();
                    controlador.anyadirUsuario(textareaUsername.getText());



                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.setTitle("");
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Campo Vacio");
            alert.setContentText("Error en la aplicacion hay algun campo vacio");
            alert.showAndWait();
        }
    }
}
