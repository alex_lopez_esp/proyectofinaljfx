package com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {

    @FXML
    private ImageView peli1;

    @FXML
    private ImageView peli2;

    @FXML
    private ImageView peli3;

    @FXML
    private ImageView peli4;

    @FXML
    private ImageView peli5;

    @FXML
    private ImageView peli6;
    private  String usuario="";

@FXML
    void initialize(){

        peli1.setImage(new Image("https://es.web.img3.acsta.net/medias/nmedia/18/67/68/58/20264670.jpg"));//PANTERA ROSA
        peli2.setImage(new Image("https://static.wikia.nocookie.net/james-camerons-avatar/images/a/a7/Caratula_de_Avatar.png/revision/latest?cb=20150218032222&path-prefix=es"));//AVATAR
        peli3.setImage(new Image("https://es.web.img3.acsta.net/pictures/18/05/10/01/53/2324639.jpg"));//ACERO PURO
        peli4.setImage(new Image("https://es.web.img2.acsta.net/medias/nmedia/18/80/30/27/19711024.jpg"));//SCREAM
        peli5.setImage(new Image("https://pics.filmaffinity.com/Cazafantasmas_M_s_all-667822266-large.jpg"));//CAZAFANTASMAS
        peli6.setImage(new Image("https://www.jerezciudad.com/imagenes/servicios/cartelera/peliculas/1341_g.jpg"));//HARRY POTTER


    }

    @FXML
    void pelicula1(MouseEvent event) {//PANTERA ROSA
    try {
        if (peli1.isPressed()) {
            Pelicula panteraRosa=new Pelicula("La pantera Rosa 2","https://es.web.img3.acsta.net/medias/nmedia/18/67/68/58/20264670.jpg","2009","Harald Zwart"
                    ,"Steve Martin\n" +
                    "Jean Reno\n" +
                    "Anne Katarine\n" +
                    "Emily Mortimer\n" +
                    "Andy García\n" +
                    "Aishwarya Rai\n" +
                    "Alfred Molina\n" +
                    "Yuki Matsuzaki\n" +
                    "John Cleese\n" +
                    "Lily Tomlin\n" +
                    "Natalie Rushman\n" +
                    "Jeremy Irons\n" +
                    "Johnny Hallyday"
                    ,92,new Sala1());

            Stage stage=new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
            Parent root=loader.load();
            MostrarPeliculas controlador=loader.getController();
            controlador.anyadirUsuario(usuario);
            Scene scene=new Scene(root);
            stage.setScene(scene);
            stage.setTitle("");
            stage.show();
            controlador.setPelicula(panteraRosa);

        }

    } catch (IOException e) {
        e.printStackTrace();
    }

    }

    @FXML
    void pelicula2(MouseEvent event) {
        try {
    if (peli2.isPressed()) {
        Pelicula avatar = new Pelicula("Avatar", "https://static.wikia.nocookie.net/james-camerons-avatar/images/a/a7/Caratula_de_Avatar.png/revision/latest?cb=20150218032222&path-prefix=es", "2009", "James Cameron", "Sam Worthington\n" +
                "Zoe Saldaña\n" +
                "Sigourney Weaver\n" +
                "Michelle Rodriguez\n" +
                "Stephen Lang\n" +
                "Giovanni Ribisi\n" +
                "Joel David Moore\n" +
                "C. C. H. Pounder\n" +
                "Wes Studi\n" +
                "Laz Alonso", 144,new Sala1());

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
        Parent root = loader.load();
       MostrarPeliculas controlador = loader.getController();
        controlador.anyadirUsuario(usuario);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("");
        stage.show();
        controlador.setPelicula(avatar);
    }

    } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void pelicula3(MouseEvent event) {
    try {
        if (peli3.isPressed()){
            Pelicula aceroPuro=new Pelicula("Acero Puro","https://es.web.img3.acsta.net/pictures/18/05/10/01/53/2324639.jpg","2011","Shawn Levy","Hugh Jackman\n" +
                    "Dakota Goyo\n" +
                    "Evangeline Lilly\n" +
                    "Anthony Mackie\n" +
                    "Kevin Durand",124,new Sala1());


            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
            Parent root = loader.load();
            MostrarPeliculas controlador = loader.getController();
            controlador.anyadirUsuario(usuario);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("");
            stage.show();
            controlador.setPelicula(aceroPuro);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }

    }

    @FXML
    void pelicula4(MouseEvent event) {
        try {
            if (peli4.isPressed()){
                Pelicula scream=new Pelicula("SCRE4M","https://es.web.img2.acsta.net/medias/nmedia/18/80/30/27/19711024.jpg","2011","Wes Craven","David Arquette\n" +
                        "Neve Campbell\n" +
                        "Courteney Cox\n" +
                        "Emma Roberts\n" +
                        "Hayden Panettiere\n" +
                        "Anthony Anderson\n" +
                        "Alison Brie\n" +
                        "Adam Brody\n" +
                        "Rory Culkin\n" +
                        "Marielle Jaffe\n" +
                        "Erik Knudsen\n" +
                        "Mary McDonnell\n" +
                        "Marley Shelton\n" +
                        "Nico Tortorella",90,new Sala1());


                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
                Parent root = loader.load();
                MostrarPeliculas controlador = loader.getController();
                controlador.anyadirUsuario(usuario);
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("");
                stage.show();
                controlador.setPelicula(scream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void pelicula5(MouseEvent event) {
        try {
            if (peli5.isPressed()){
                Pelicula cazaFantasmas=new Pelicula("CazaFantasmas","https://pics.filmaffinity.com/Cazafantasmas_M_s_all-667822266-large.jpg","2021","Jason Reitman","Bill Murray\n" +
                        "Dan Aykroyd\n" +
                        "Ernie Hudson\n" +
                        "Sigourney Weaver\n" +
                        "Annie Potts\n" +
                        "Paul Rudd\n" +
                        "Finn Wolfhard\n" +
                        "Celeste O'Connor\n" +
                        "Carrie Coon\n" +
                        "Logan Kim\n" +
                        "Mckenna Grace\n" +
                        "Marlon Kazadi\n" +
                        "Sydney Mae Diaz\n" +
                        "Tracy Letts",124,new Sala1());


                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
                Parent root = loader.load();
                MostrarPeliculas controlador = loader.getController();
                controlador.anyadirUsuario(usuario);
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("");
                stage.show();
                controlador.setPelicula(cazaFantasmas);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void pelicula6(MouseEvent event) {
        try {
            if (peli6.isPressed()){
                Pelicula harryPotter=new Pelicula("Harry potter y las reliquias","https://www.jerezciudad.com/imagenes/servicios/cartelera/peliculas/1341_g.jpg","2010","David Yates","Daniel Radcliffe\n" +
                        "Rupert Grint\n" +
                        "Emma Watson\n" +
                        "Ralph Fiennes\n" +
                        "Michael Gambon\n" +
                        "Alan Rickman\n" +
                        "Robbie Coltrane\n" +
                        "Helena Bonham Carter\n" +
                        "Tom Felton\n" +
                        "Jason Isaacs\n" +
                        "Evanna Lynch\n" +
                        "David Thewlis\n" +
                        "Brendan Gleeson\n" +
                        "Julie Walters\n" +
                        "Bill Nighy\n" +
                        "Natalia Tena\n" +
                        "Domhnall Gleeson\n" +
                        "Clemence Poesy\n" +
                        "James y Oliver Phelps\n" +
                        "Bonnie Wright\n" +
                        "Imelda Staunton",146,new Sala1());


                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("mostrarPeliculas.fxml"));
                Parent root = loader.load();
                MostrarPeliculas controlador = loader.getController();
                controlador.anyadirUsuario(usuario);
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("");
                stage.show();
                controlador.setPelicula(harryPotter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
    @FXML
    void anyadirUsuario(String nombre){
        usuario=nombre;

    }



}