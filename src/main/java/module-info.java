module com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez to javafx.fxml;
    exports com.example.tfjx_de_alex_lopez.tfjx_de_alex_lopez;
}